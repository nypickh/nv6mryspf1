<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/_assets/site.css" rel="stylesheet">
    <link href="/_assets/favicon.png" rel="icon" type="image/png">
    <title>CS107E Assignment 5: Keyboard and Simple Shell</title>
  </head>

<body>
  <!-- Include this HTML partial to set up navbar -->

<nav class="navbar navbar-inverse" role="navigation">
  <div class="navbar-header">
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
    <!-- hamburger -->
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>

  <img class="navbar-brand navbar-brand-image" src="/_assets/berry.png" >
  <a class="navbar-brand" href="/">CS107e Winter 2020</a>
  </div>

  <div class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/">Home</a></li>
      <li><a href="/assignments">Assignments</a></li>
      <li><a href="/labs">Labs</a></li>
      <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            Resources
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/about/">About the course</a></li>
            <li><a href="/policies/">Policies</a></li>
            <li><a href="/schedule/">Schedule</a></li>
            <li><a href="/guides/">Guides</a></li>
            <li><a href="/project_gallery/">Project gallery</a></li>
            <li><a href="/demos/">External demos</a></li>
            <li><a href="/resources/">External resources</a></li>
          </ul>
        </li>
      <li><a href="https://github.com/cs107e/cs107e.github.io">Repository</a></li>
    </ul>
  </div>
</nav>


  <div class="container" style="max-width:55em;">
    <h1 class="title">Assignment 5: Keyboard and Simple Shell</h1><hr>
    
  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" id="main_for_toc">
    
<p><em>Written by Philip Levis, updated by Julie Zelenski</em></p>

<!-- If duedate set in front_matter, use that, otherwise calculate based on n*weeks + assign1 due -->

<p><b>Due: Tuesday, February 18 at  6:00 pm</b></p>

<p><img src="images/pi_shell.png" alt="Pi shell screenshot" width="80%" /></p>

<h2 id="goals">Goals</h2>

<p>For this week’s assignment, you will implement a PS/2 keyboard driver and implement a simple command-line shell. You will then be able to type commands and execute them on your Pi. Neat!</p>

<p>In completing this assignment you will have:</p>

<ul>
  <li>written code that interfaces with an input device. When you next download a device driver, you will say “I have a decent idea how that code operates”</li>
  <li>seen the design of a complex interface into hierarchical levels and appreciated its benefits</li>
  <li>implemented a simple command-line interpreter</li>
  <li>explored the use of C function pointers for callbacks and command dispatch</li>
</ul>

<p>This is a fun assignment, and brings us back to using physical devices and making them do cool things. These additions are the first steps toward turning your humble little Raspberry Pi into a standalone personal computer.</p>

<h2 id="get-starter-files">Get starter files</h2>
<p>Change to the <code class="highlighter-rouge">cs107e.github.io</code> repository in your <code class="highlighter-rouge">cs107e_home</code> and do a <code class="highlighter-rouge">git pull</code> to ensure your courseware files are up to date.</p>

<p>To get the assignment starter code, change to your local repository, fetch any changes from the remote and switch to the assignment basic branch:</p>
<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>$ cd ~/cs107e_home/assignments
$ git fetch origin
$ git checkout assign5-basic
</code></pre></div></div>

<p>Read the <code class="highlighter-rouge">Makefile</code> for information on how to reuse the modules you have written for previous assignments.</p>

<p>Verify that your project has up-to-date versions of
your modules <code class="highlighter-rouge">gpio.c</code>, <code class="highlighter-rouge">timer.c</code>, <code class="highlighter-rouge">strings.c</code>, <code class="highlighter-rouge">printf.c</code>, <code class="highlighter-rouge">backtrace.c</code> and <code class="highlighter-rouge">malloc.c</code>.  If you are missing updates made on a different branch, use <code class="highlighter-rouge">git merge</code> to incorporate those changes. For example, if you made a regrade submission on <code class="highlighter-rouge">assign3-basic</code> and want to add those changes into <code class="highlighter-rouge">assign5-basic</code>, have <code class="highlighter-rouge">assign5-basic</code> checked out and use <code class="highlighter-rouge">git merge assign3-basic</code>.</p>

<p>The starter project contains the module files <code class="highlighter-rouge">keyboard.c</code> and <code class="highlighter-rouge">shell.c</code>, the application program <code class="highlighter-rouge">apps/uart_shell.c</code> and the test program <code class="highlighter-rouge">tests/test_keyboard.c</code>. You will edit <code class="highlighter-rouge">keyboard.c</code> and <code class="highlighter-rouge">shell.c</code> to implement the required functions. You can add tests to <code class="highlighter-rouge">tests/test_keyboard.c</code>. The program <code class="highlighter-rouge">apps/uart_shell.c</code> is used unchanged as a sample application which tests your shell program.</p>

<p>The <code class="highlighter-rouge">make install</code> target of the Makefile builds and runs the sample application <code class="highlighter-rouge">apps/uart_shell.bin</code>. The <code class="highlighter-rouge">make test</code> target builds and runs the test program <code class="highlighter-rouge">tests/test_keyboard.bin</code>. With no argument, <code class="highlighter-rouge">make</code> will build both, but not run.</p>

<h2 id="basic-part">Basic part</h2>

<h3 id="keyboard-driver">Keyboard driver</h3>

<p>You are to implement the <code class="highlighter-rouge">keyboard</code> module that interfaces with a PS/2 keyboard. The exported functions are:</p>

<ul>
  <li><code class="highlighter-rouge">void keyboard_init(unsigned int clock_gpio, unsigned int data_gpio)</code></li>
  <li><code class="highlighter-rouge">unsigned char keyboard_read_next(void)</code></li>
  <li><code class="highlighter-rouge">key_event_t keyboard_read_event(void)</code></li>
  <li><code class="highlighter-rouge">key_action_t keyboard_read_sequence(void)</code></li>
  <li><code class="highlighter-rouge">unsigned char keyboard_read_scancode(void)</code></li>
</ul>

<p>Our <a href="https://github.com/cs107e/cs107e.github.io/blob/master/cs107e/include/keyboard.h">keyboard.h</a> documents the operation of each function.</p>

<p>The design of the keyboard module is worth a pause to understand and appreciate. The functionality is arranged hierarchically, each routine building on the next. The bottom level routine reads a raw scancode, the next level gathers a sequence of scancodes into one logical key action, the higher level routines translate those actions into key events and typed characters.</p>

<p>The layered interface cleanly supports the needs of different clients. A client that simply wants typed characters might need only the top level <code class="highlighter-rouge">keyboard_read_next</code>; a client that reacts to up and down events also accesses the mid level <code class="highlighter-rouge">keyboard_read_event</code>.</p>

<p>The hierarchical design also eases the job of the implementor. Each level focuses on a discrete part of the operation and delegates tasks above and below to other functions. This makes each function simpler to implement and test. Your implementation plan of attack is to start at the bottom and work your way upward.</p>

<h4 id="1-read-and-validate-scancodes">1) Read and validate scancodes</h4>

<p>The bottom level <code class="highlighter-rouge">keyboard_read_scancode</code> handles the nitty-gritty of the PS2/2 protocol.  You got a start on implementing this function during lab5, copy that work into <code class="highlighter-rouge">keyboard.c</code> now. The entire keyboard module rests on this cornerstone, so your first task is to finish it off and ensure it is reliable and robust.</p>

<p>A PS/2 scancode is an 11-bit packet organized in 8-odd-1 format. The first bit, the start bit, is low. The next 8
bits are data bits, least significant bit first. The following bit is a parity
bit. The PS/2 protocol uses odd parity, which means that there should be an odd number of 1s among the data and parity bits. The 11th and final bit is the stop bit, which is
always high.</p>

<p><img src="images/ps2.png" alt="PS/2 Packet Format" /></p>

<p>The return value from <code class="highlighter-rouge">keyboard_read_scancode</code> is the 8 data bits from a well-formed packet. It is important to detect and recover from transmission errors. Check the value of each start, parity, and stop bit. If you detect an erroneous bit, discard the partial scancode and retry reading a new scancode from the beginning.  Discard as many invalid tries as necessary until you receive a valid scancode.</p>

<p>In a similar vein, a dropped bit or discarded partial read could cause your driver to become desynchronized. When that happens your driver can get stuck, trying to read a scancode byte starting mid-packet and waiting for further bits to arrive that aren’t forthcoming. One way to resynchronize is using a simple timeout reset. Use
your timer module to note if the current clock edge occurred more than
3ms after the previous one, and if so, reset the state and
assume the current clock edge is for a start bit. This small effort provides additional robustness to combat flaky connections and hardware blips.</p>

<!-- This partial will open warning/danger callout box  -->

<div style="background-color:#ffcccc; color:#993333; border-left: 5px solid #993333;margin: 5px 25px; padding: 5px;">

  <p><strong>Timing is everything!</strong>: The timing of the PS/2 protocol has to be strictly followed. The keyboard sends the bits rapid-fire and you must catch each bit as it arrives. Once your driver sees the falling clock edge for the start bit, it needs to stay on task to read each subsequent bit.  There is not time between clock edges to complete a call to a complex function like printf.  Save any debug printing for after you read the entire sequence.</p>
</div>

<p>The first test in <code class="highlighter-rouge">tests/test_keyboard.c</code>  will read and echo scancodes. Use this test to verify your basic scancode functionality.</p>

<h4 id="2-gather-sequence-into-key-action">2) Gather sequence into key action</h4>

<p>The next level function  <code class="highlighter-rouge">keyboard_read_sequence</code> gathers a sequence of scancodes into one logical key action.  A key action is the press or release of a single key.</p>

<p>When you press a key, the PS/2 keyboard sends the scancode for that key. When you release
the key, it sends a two-byte sequence: <code class="highlighter-rouge">0xF0</code> (the “break” code) followed by the key’s scancode. For example, typing <code class="highlighter-rouge">z</code> will cause the keyboard to send <code class="highlighter-rouge">0x1A</code> and releasing <code class="highlighter-rouge">z</code> will
cause the keyboard to send <code class="highlighter-rouge">0xF0</code>, <code class="highlighter-rouge">0x1A</code>.</p>

<p>The press or release of an extended key sends a sequence with the extra byte <code class="highlighter-rouge">0xE0</code> inserted at the front. For example, pressing the right Control key sends the sequence
<code class="highlighter-rouge">0xE0</code>, <code class="highlighter-rouge">0x14</code> and releasing sends <code class="highlighter-rouge">0xE0</code>, <code class="highlighter-rouge">0xF0</code>, <code class="highlighter-rouge">0x14</code>.</p>

<p>The <code class="highlighter-rouge">keyboard_read_sequence</code> function reads the sequence (1, 2 or 3 bytes depending on context) and translates it into a <code class="highlighter-rouge">key_action_t</code> struct which reports the type of action (press or release) and which key was involved.</p>

<p>Use the test program <code class="highlighter-rouge">tests/test_keyboard.c</code> to verify the operation of this function before moving on.</p>

<h4 id="3-process-key-events">3) Process key events</h4>

<p>There are additional type and constant definitions used starting at this level.  Review <a href="https://github.com/cs107e/cs107e.github.io/blob/master/cs107e/include/keyboard.h">keyboard.h</a> for the definitions of <code class="highlighter-rouge">keyboard_modifiers_t</code> and <code class="highlighter-rouge">key_event_t</code> and <a href="https://github.com/cs107e/cs107e.github.io/blob/master/cs107e/include/ps2.h">ps2.h</a> for the definition of <code class="highlighter-rouge">ps2_key_t</code> and keycode constants.</p>

<p>The mid level routine <code class="highlighter-rouge">keyboard_read_event</code> processes key actions into key events. It calls <code class="highlighter-rouge">keyboard_read_sequence</code> to get a <code class="highlighter-rouge">key_action_t</code> and packages the action into a <code class="highlighter-rouge">key_event_t</code> struct which includes the state of keyboard modifiers and the PS/2 key that was acted upon.</p>

<p>The <code class="highlighter-rouge">modifiers</code> field of a <code class="highlighter-rouge">key_event_t</code> reports which modifier keys are in effect.  The state of all modifier keys is compactly represented using a <em>bit set</em>. The <code class="highlighter-rouge">keyboard_modifiers_t</code> enumeration type designates a particular bit for each modifier key.  If a bit is set in <code class="highlighter-rouge">modifiers</code>, this indicates the corresponding modifier key is currently held down or in the active state. If the bit is clear, it indicates the modifier is inactive.</p>

<p>The PS/2 protocol does not provide a way to ask the keyboard which modifiers are active, instead your driver must track the modifier state itself. A simple approach is a static variable in your keyboard module that you update in response to modifier key events. The Shift, Control, and Alt modifiers are active iff the modifier key is currently down. Caps Lock operates differently in that its setting is “sticky”. A press makes Caps Lock active and that persists until a subsequent press inverts the state.</p>

<p>To identify which characters can be produced by a given key, we provide an array to use as a <em>lookup table</em>. Review the definition of the <code class="highlighter-rouge">ps2_keys</code> array in <a href="https://github.com/cs107e/cs107e.github.io/blob/master/cs107e/src/ps2.c">ps2.c</a>.  The array is indexed by scancode. The <strong>A</strong> key on PS/2 keyboard generates scancode <code class="highlighter-rouge">0x1C</code>; array element <code class="highlighter-rouge">ps2_keys[0x1C]</code> holds the PS/2 key information for this key.</p>

<p>Use the functions in <code class="highlighter-rouge">test/test_keyboard.c</code> to verify your processing of key events before moving on.</p>

<h4 id="4--produce-ascii-characters">4)  Produce ASCII characters</h4>

<p>You now have all of the pieces needed to implement the final top-level routine <code class="highlighter-rouge">keyboard_read_next</code>.  This function calls <code class="highlighter-rouge">keyboard_read_event</code> to get the next key press event and produces the character corresponding to the key that was typed.</p>

<p>The return value is the ASCII character produced by an ordinary key or a  value designated for a special key such as Escape or F9. The character produced by a key is determined by its <code class="highlighter-rouge">ps2_key_t </code>entry in the lookup table.</p>

<p>A <code class="highlighter-rouge">ps2_key_t</code> has two fields for each key, <code class="highlighter-rouge">ch</code> and <code class="highlighter-rouge">other_ch</code>, which correspond to the unmodified and modified character produced by the key.  The <strong>A</strong> key produces <code class="highlighter-rouge">{ 'a', 'A' }</code>. The <strong>Four</strong> key produces <code class="highlighter-rouge">{ '4', '$' }</code>. Keys such as <strong>Tab</strong> that do not produce a different character when modified have a 0 in the <code class="highlighter-rouge">other_ch</code> slot, e.g.as <code class="highlighter-rouge">{'\t', 0}</code>.</p>

<p>Your driver should handle all keys shown in this keyboard diagram.</p>

<p><img src="images/scancode.gif" alt="PS/2 Scancodes" /></p>

<p>Typing an ordinary key produces an ASCII character. The ordinary keys are:</p>
<ul>
  <li>All letters, digits, and punctuation keys</li>
  <li>Whitespace keys (Space, Tab, Return)</li>
</ul>

<p>Typing a special key produces its designated value. These values are greater than 0x90 to distinguish from ASCII values. The special keys are:</p>
<ul>
  <li>Escape</li>
  <li>Function keys F1-F12</li>
  <li>Backspace key (sometimes marked with ← or ⌫)</li>
</ul>

<p>Press or release of a modifier key changes the event modifiers.  No character or code is produced.  The modifier keys are:</p>
<ul>
  <li>Shift, Caps Lock, Alt, Control</li>
</ul>

<p>A change in modifiers can affect the character produced by future typed keys. Our keyboard translation layer does not produce modified characters based on state of Alt or Control, only for Shift and Caps Lock. When the Shift modifier is active, <code class="highlighter-rouge">other_ch</code> is produced when typing a key that has an <code class="highlighter-rouge">other_ch</code> entry. If Caps Lock is active, <code class="highlighter-rouge">other_ch</code> is produced only for the alphabetic keys. Caps Lock has no effect on digits, punctuation, and other keys. If Shift and Caps Lock applied together, Shift “wins”, e.g. <code class="highlighter-rouge">other_ch</code> is produced. (Caps Lock and Shift together do not invert letters to lowercase).</p>

<p>If you are using a Mac, Keyboard Viewer (accessible from menu bar) is a handy tool for visualizing the character produced for a given key combination. Try it out!
If you are still unsure how to handle a particular case,
experiment with our reference implementation of the keyboard
using the test application from lab.</p>

<h3 id="simple-shell">Simple shell</h3>
<p>With a keyboard as input device, your Pi has gone interactive! The simple shell application allows the user to enter commands and control the Pi without needing to plug another computer into it.</p>

<p>The video below is a demonstration of our reference shell. The user is typing on a PS/2 keyboard connected to the Pi and the shell output is displaying over uart to a Mac laptop.</p>

<iframe width="625" height="400" src="https://www.youtube-nocookie.com/embed/7m_nOmzCHiw?modestbranding=1&amp;version=3&amp;playlist=7m_nOmzCHiw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>

<h4 id="1-review-shell-interface-and-starter-code">1) Review shell interface and starter code</h4>

<p>A shell, such as <code class="highlighter-rouge">bash</code> or <code class="highlighter-rouge">zsh</code>, is a program that operates as a command-line interpreter. The program sits in a loop, reading a command typed by the user and then executing it.</p>

<p>The starter code for <code class="highlighter-rouge">shell_run</code> demonstrates the standard read-eval-print loop that is at the heart of an interpreter. Here it is in pseudocode:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>loop forever
    display prompt
    read line of input from user
    evaluate command (parse and execute)
</code></pre></div></div>

<p>The public functions you will implement in shell are:</p>

<ul>
  <li><code class="highlighter-rouge">void shell_readline(char buf[], size_t bufsize)</code></li>
  <li><code class="highlighter-rouge">int shell_evaluate(const char *line)</code></li>
</ul>

<p>The <code class="highlighter-rouge">shell_readline</code> function reads a command typed by the user. The <code class="highlighter-rouge">shell_evaluate</code> executes that command.  Review the documentation for these operations in the header file <a href="https://github.com/cs107e/cs107e.github.io/blob/master/cs107e/include/shell.h">shell.h</a>.</p>

<p>One important detail of the shell’s interface to draw your attention to is how the shell provides flexibility in the output device. The <code class="highlighter-rouge">shell_init</code> function takes a <em>function pointer</em> argument to be supplied by the client. Whenever the shell wants to display output, it calls the client’s function pointer.</p>

<p>The <code class="highlighter-rouge">apps/uart_shell.c</code> application program initializes the shell with <code class="highlighter-rouge">shell_init(printf)</code>; this configures the shell to send its output to the serial uart interface.
In assignment 6, you’ll write a <code class="highlighter-rouge">console_printf</code> function that draws to a HDMI monitor. Initializing the shell with <code class="highlighter-rouge">shell_init(console_printf)</code> configures the shell to call this function instead to display output on the graphical console.</p>

<p>As a rule, your shell should display all output by calling <code class="highlighter-rouge">shell_printf</code> (which is set to the client’s function pointer), never a direct call to a particular output function.  This applies to all shell output, whether it be the shell prompt, displaying the result from a command, or responding with an error message to an invalid request.</p>

<h4 id="2--read-line">2)  Read line</h4>

<p><code class="highlighter-rouge">shell_readline</code> reads the characters typed on the keyboard by the user and writes the line of input into a buffer. The user indicates the end of the line by typing Return (<code class="highlighter-rouge">\n</code>).  You’ll find this task fairly straightforward given your awesome keyboard driver.</p>

<p>Handling backspace adds a slight twist. When the user types Backspace (<code class="highlighter-rouge">\b</code>), the shell deletes the last
character typed on the current line.  Removing it from the buffer is simple enough, but how to un-display it? If you output a backspace character, e.g. <code class="highlighter-rouge">shell_printf("%c", '\b')</code> , it moves the cursor backwards one position. If you back up, output a space, and then back up again, you will have effectively “erased” a character. (Wacky, but it works!)</p>

<p>There are two error conditions that <code class="highlighter-rouge">shell_readline</code> should detect:</p>
<ul>
  <li>disallow typing more characters than fit in the buffer</li>
  <li>disallow backspacing through prompt or to previous line</li>
</ul>

<p>Reject the attempt and call the provided <code class="highlighter-rouge">shell_bell</code> function to get an audio/visual beep.</p>

<p><code class="highlighter-rouge">shell_readline</code> is a great way to exercise your shiny new keyboard driver!</p>

<h4 id="3-parse-command-line">3) Parse command line</h4>

<p><code class="highlighter-rouge">shell_evaluate</code> first takes the line entered by the user and parses it into a command and arguments. The parsing job is all about string manipulation, which is right up your alley after assign 3.</p>

<ul>
  <li>Divide the line into an array of tokens. A token consists of a sequence of non-space chars.</li>
  <li>Ignore/skip all whitespace in between tokens as well as leading and trailing whitespace. Whitespace includes space, tab, and newline.</li>
  <li>The first token is the name of the command to execute, the subsequent tokens are the arguments to the command.</li>
</ul>

<p>When tokenizing, be sure to take advantage of the nifty functions you implemented in your <code class="highlighter-rouge">strings</code> and <code class="highlighter-rouge">malloc</code> modules. They will be helpful!</p>

<h4 id="4--execute-command">4)  Execute command</h4>
<p>Now that you have the command name and arguments, you’re ready to evaluate it.</p>

<ul>
  <li>Look up the function pointer for the command by name. The command table associates a command name string with its function pointer.
    <ul>
      <li>If no matching command is found, output message <code class="highlighter-rouge">error: no such command 'binky'.</code></li>
    </ul>
  </li>
  <li>Call the function pointer, passing the array of tokens and the count of tokens. The first element in the array is the command name, the subsequent elements are the arguments to the command.</li>
  <li>The return value of the command is used as the return value for <code class="highlighter-rouge">shell_evaluate</code>.</li>
</ul>

<p>There is a command table started in <code class="highlighter-rouge">shell.c</code>. You will modify the table as you add commands. Each entry in the table is a <code class="highlighter-rouge">command_t</code> struct as defined in 
<a href="https://github.com/cs107e/cs107e.github.io/blob/master/cs107e/include/shell_commands.h">shell_commands.h</a>. A command function pointer takes two parameters: <code class="highlighter-rouge">argv</code> is an array of <code class="highlighter-rouge">char *</code>, i.e. an array of strings, and <code class="highlighter-rouge">argc</code> is the count of elements in the  <code class="highlighter-rouge">argv</code> array. A command function returns an int to indicate success or failure. The result is 0 if the command executed successfully, or nonzero otherwise.</p>

<p>Your shell has five commands:</p>

<ul>
  <li><code class="highlighter-rouge">int cmd_echo(int argc, const char *argv[])</code></li>
  <li><code class="highlighter-rouge">int cmd_help(int argc, const char *argv[])</code></li>
  <li><code class="highlighter-rouge">int cmd_reboot(int argc, const char* argv[])</code></li>
  <li><code class="highlighter-rouge">int cmd_peek(int argc, const char* argv[])</code></li>
  <li><code class="highlighter-rouge">int cmd_poke(int argc, const char* argv[])</code></li>
</ul>

<p>We’ve implemented <code class="highlighter-rouge">cmd_echo</code> as an example. This command simply echoes its arguments:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>Pi&gt; echo Hello, world!
Hello, world!
</code></pre></div></div>

<p>The additional commands you are to implement are:</p>

<ul>
  <li>
    <p><strong>help</strong></p>

    <p>Without any arguments, <code class="highlighter-rouge">help</code> prints a list of all available commands
  along with their description in the following format:</p>

    <div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>  Pi&gt; help
  cmd1: description
  cmd2: description
</code></pre></div>    </div>

    <p>If an argument is given, <code class="highlighter-rouge">help</code> prints the description for that command,
  or an error message if the command doesn’t exist:</p>

    <div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>  Pi&gt; help reboot
  reboot:  reboot the Raspberry Pi back to the bootloader
  Pi&gt; help please
  error: no such command `please`.
</code></pre></div>    </div>
  </li>
  <li>
    <p><strong>reboot</strong></p>

    <p>This command restarts your Pi by calling the <code class="highlighter-rouge">pi_reboot</code> function from the <code class="highlighter-rouge">pi</code> module of <code class="highlighter-rouge">libpi</code>. See ya back at the bootloader!</p>
  </li>
  <li>
    <p><strong>peek</strong></p>

    <p>This command takes one argument: <code class="highlighter-rouge">[address]</code>.  It prints the 4-byte value stored at memory address <code class="highlighter-rouge">address</code>.</p>

    <p>Example (assume address 0xFFFC contains the number 0x12345678):</p>

    <div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>  Pi&gt; peek 0xFFFC
  0x0000fffc:  12345678
</code></pre></div>    </div>

    <p>Hint: the <code class="highlighter-rouge">strtonum</code> function from your strings module is handy for converting strings to numbers. If the address argument is missing or cannot be converted, <code class="highlighter-rouge">peek</code> prints an error message:</p>

    <div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>  Pi&gt; peek
  error: peek expects 1 argument [address]
  Pi&gt; peek bob
  error: peek cannot convert 'bob'
</code></pre></div>    </div>

    <p>Recall that the ARM architecture is not keen to load/store on an unaligned address. A 4-byte value can only be read starting from an address that is a multiple of 4. If the user asks to peek (or poke) at an unaligned address, respond with an error message:</p>

    <div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>  Pi&gt; peek 7
  error: peek address must be 4-byte aligned
</code></pre></div>    </div>
  </li>
  <li>
    <p><strong>poke</strong></p>

    <p>This command takes two arguments: <code class="highlighter-rouge">[address] [value]</code>.  The poke function stores <code class="highlighter-rouge">value</code> into the memory at <code class="highlighter-rouge">address</code>.</p>

    <p>Example (assume 0xFFFC currently contains the number 0x12345678):</p>

    <div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>  Pi&gt; peek 0xFFFC
  0x0000fffc:  123456789
  Pi&gt; poke 0xFFFC 1
  Pi&gt; peek 0xFFFC
  0x0000fffc:  00000001
  Pi&gt; poke 0xFFFC 0
  Pi&gt; peek 0xFFFC
  0x0000fffc:  00000000
</code></pre></div>    </div>

    <p><code class="highlighter-rouge">strtonum</code> will again be handy.  If either argument is missing or cannot be converted, <code class="highlighter-rouge">poke</code> prints an error message:</p>

    <div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>  Pi&gt; poke 0xFFFC
  error: poke expects 2 arguments [address] [value]
  Pi&gt; poke fred 5
  error: poke cannot convert 'fred'
  Pi&gt; poke 0xFFFC wilma
  error: poke cannot convert 'wilma'
</code></pre></div>    </div>

    <p>You can now turn a GPIO
  pin on and off by entering shell commands!</p>

    <div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>  Pi&gt; poke 0x20200010 0x200000
  Pi&gt; poke 0x20200020 0x8000
  Pi&gt; poke 0x2020002C 0x8000
</code></pre></div>    </div>

    <p>Review the
  <a href="/readings/BCM2835-ARM-Peripherals.pdf#page=90">BCM2835 manual</a>
  to see what is stored at these addresses. What do the above
  commands do? Hint: the ACT LED on the Pi
  is GPIO pin 47.</p>

    <p>Check out the
  <a href="https://en.wikipedia.org/wiki/PEEK_and_POKE">Wikipedia article on peek and poke</a> if you’re curious.</p>
  </li>
</ul>

<h2 id="testing-and-debugging">Testing and debugging</h2>
<p>As usual, the effort you put into writing good tests will be evaluated along with your code submission. An interactive program such as this one adds new challenges for testing. Your inventive solutions to overcoming these challenges are welcome!</p>

<p>The given code in <code class="highlighter-rouge">tests/test_keyboard.c</code> program has functions that test each layer of the keyboard module. These functions simply echo the data returned by the keyboard module. You must manually verify the correctness of the output.</p>

<p>The <code class="highlighter-rouge">test_keyboard_assert</code> function demonstrates an example approach for an assert-based test where you coordinate with the user to provide the keyboard input.</p>

<p>The <code class="highlighter-rouge">shell</code> module is intended to be run interactively and does not lend itself well to assert-based testing. This doesn’t mean you should eschew testing it, but you will have to be more creative in how you proceed. A former student set up an array of commands to be used as a test script and then fed each line to <code class="highlighter-rouge">shell_evaluate</code> in sequence and verified that the output was as expected, which I thought was pretty clever.</p>

<p>The <code class="highlighter-rouge">shell_readline</code> function is a particularly sticky one. Given that the simulator does not emulate the peripherals, debugging code that requires keyboard input under gdb is a no-go. Extending from the former student’s test script plan, you could have a string of characters to force feed into <code class="highlighter-rouge">shell_readline</code> and instead of calling <code class="highlighter-rouge">keyboard_read_next</code>, you take the next char from your test string.</p>

<h2 id="extension-editing-and-history">Extension: editing and history</h2>

<p>Create an <code class="highlighter-rouge">assign5-extension</code> branch for this part of the work and make a separate pull request from your basic submission.</p>

<p>The extension consists of two parts. You should do both parts.</p>

<ol>
  <li>
    <p><strong>Command-line editing</strong></p>

    <p>Implement the left and right arrow keys to move the cursor within the current line and allow inserting and deleting characters at the point of the cursor. What other editing features might be nice to have:  Ctrl-a and Ctrl-e to move the cursor to the first and last character of the line? Ctrl-u to delete the entire line? Implement your favorite vim/emacs/editor must-haves!</p>
  </li>
  <li>
    <p><strong>Command history</strong></p>

    <p>Number the commands entered starting from 1 and maintain a rolling history of the last 10 commands entered. Change the prompt to include the command number of the current line. Add a <code class="highlighter-rouge">history</code> command that displays the history of recent commands, each prefixed with its command number. See the example below:</p>

    <div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code> [1] Pi&gt; help echo
 echo: &lt;...&gt; echo user input to the screen
 [2] Pi&gt; echo cs107e rocks
 cs107e rocks
 [3] Pi&gt; history
   1 help echo
   2 echo cs107e rocks
   3 history
</code></pre></div>    </div>

    <p>Implement the up and down arrow keys to access commands from the history. Typing the up arrow key changes the current line to display the command from the history that is previous to the one on the current line. Typing a down array changes the current line to display the command that ran <em>after</em> the one on the current line, or whatever the user had typed until he/she typed up. Use the <code class="highlighter-rouge">shell_beep</code> when the user tries to move beyond either end of the history.</p>

    <p>What other history features might be handy? <code class="highlighter-rouge">!!</code> to repeat the last command? <code class="highlighter-rouge">!ze</code> to repeat the most recent command matching the specified prefix <code class="highlighter-rouge">ze</code>?</p>

    <p>If you didn’t already know that your regular shell includes editing and history features like these, now is a great time to pick up a few new tricks to help boost your productivity!</p>
  </li>
</ol>

<p>If you wrote the disassemble extension for assign3 (or you want to do it now!), a cute option is to integrate a <code class="highlighter-rouge">disassemble [addr]</code> command into your shell. The fun never stops!</p>

<h2 id="submit">Submit</h2>
<p>The deliverables for <code class="highlighter-rouge">assign5-basic</code> are:</p>

<ul>
  <li>Implementation of the <code class="highlighter-rouge">keyboard.c</code> and <code class="highlighter-rouge">shell.c</code> modules</li>
  <li>Your comprehensive keyboard tests in <code class="highlighter-rouge">tests/test_keyboard.c</code></li>
</ul>

<p>Submit the finished version of your assignment by making a git “pull request”. Make separate pull requests for your basic and extension submissions.</p>

<p>The automated checks make sure that we can run your C
code and test and grade it properly, including swapping your tests for
ours.</p>

<p>CI verifies that:</p>

<ul>
  <li>
    <p><code class="highlighter-rouge">apps/uart_shell.c</code> is unchanged</p>
  </li>
  <li>
    <p><code class="highlighter-rouge">make</code> and <code class="highlighter-rouge">make test</code> successfully build</p>
  </li>
  <li>
    <p><code class="highlighter-rouge">make test</code> also successfully builds with the unchanged version of the test program in the starter</p>
  </li>
</ul>

<h2 id="grading">Grading</h2>
<p>To grade this assignment, we will:</p>

<ul>
  <li>Verify that your submission builds correctly, with no warnings. Warnings and/or build errors result in automatic deductions. Clean build always!</li>
  <li>Run automated tests that exercise the functionality of your <code class="highlighter-rouge">keyboard.c</code> and <code class="highlighter-rouge">shell.c</code> modules. These tests will touch on all required features of the module.</li>
  <li>Our automated testing requires that your shell has absolute consistency in calling <code class="highlighter-rouge">shell_printf</code> for all shell output. Double-check that you are compliant. Also be sure to have removed or commented out all <code class="highlighter-rouge">printf</code> calls inserted for debugging.</li>
  <li>Go over the tests you added to <code class="highlighter-rouge">tests/test_keyboard.c</code> and evaluate them for thoughtfulness and completeness in coverage.</li>
  <li>Review your code and provide feedback on your design and style choices.</li>
</ul>

<p><img src="https://media.giphy.com/media/l2SqesOmvIdj1RExq/giphy.gif" alt="nice job!" /></p>

  </div>
  <div class="toc-column col-lg-2 col-md-2 col-sm-2 hidden-xs">
    <div id="toc" class="toc" data-spy="affix" data-offset-top="0"></div>
  </div> 
</div>

  <script src="/_assets/tocbot.min.js"></script>
  <link rel="stylesheet" href="/_assets/tocbot.css">

  <script>
    tocbot.init({
      // Where to render the table of contents.
      tocSelector: '#toc',
      // Where to grab the headings to build the table of contents.
      contentSelector: '#main_for_toc',
      // Which headings to grab inside of the contentSelector element.
      headingSelector: 'h2, h3, h4',
    });
  </script>



  </div>
  <div class="footer navbar-default navbar-static-bottom">
    <p style="font-size: 65%; color:green; text-align:center;">
      <i>CS107e Winter 2020 &middot; Site generated 2020-04-19 20:35</i>
  </p>
</div> 
</body>

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</html>