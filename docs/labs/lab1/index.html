<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/_assets/site.css" rel="stylesheet">
    <link href="/_assets/favicon.png" rel="icon" type="image/png">
    <title>CS107E Lab 1: Setup your Raspberry Pi</title>
  </head>

<body>
  <!-- Include this HTML partial to set up navbar -->

<nav class="navbar navbar-inverse" role="navigation">
  <div class="navbar-header">
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
    <!-- hamburger -->
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>

  <img class="navbar-brand navbar-brand-image" src="/_assets/berry.png" >
  <a class="navbar-brand" href="/">CS107e Winter 2020</a>
  </div>

  <div class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/">Home</a></li>
      <li><a href="/assignments">Assignments</a></li>
      <li><a href="/labs">Labs</a></li>
      <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            Resources
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/about/">About the course</a></li>
            <li><a href="/policies/">Policies</a></li>
            <li><a href="/schedule/">Schedule</a></li>
            <li><a href="/guides/">Guides</a></li>
            <li><a href="/project_gallery/">Project gallery</a></li>
            <li><a href="/demos/">External demos</a></li>
            <li><a href="/resources/">External resources</a></li>
          </ul>
        </li>
      <li><a href="https://github.com/cs107e/cs107e.github.io">Repository</a></li>
    </ul>
  </div>
</nav>


  <div class="container" style="max-width:55em;">
    <h1 class="title">Lab 1: Setup your Raspberry Pi</h1><hr>
    
  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" id="main_for_toc">
    
<p><em>Lab written by Pat Hanrahan</em></p>

<h2 id="goals">Goals</h2>

<p>During this lab you will:</p>

<ul>
  <li>Learn how to cross-develop on your computer for the Raspberry Pi’s ARM processor.</li>
  <li>Learn how to use a breadboard with LEDs and buttons.</li>
  <li>Learn how to download and run bare metal programs on the Raspberry Pi.</li>
</ul>

<h2 id="how-does-lab-work">How does lab work?</h2>

<p>When you arrive at lab, find a partner and introduce yourself to one another.
Together you will tackle the exercises below.
Everyone is encouraged to collaborate with other labmates 
to share insights and offer each other useful tips.
The instructor and TA will circulate
to offer advice and answers 
so as to keep everyone progressing smoothly.</p>

<p>Lab is a time to experiment and explore. After we introduce topics in readings/lectures, you’ll do guided exercises in lab to further your understanding of the material, get hand-on practice with the tools in a supported environment, and ready yourself to succeed at this week’s assignment.</p>

<p>Bare metal programming requires precision. A trivial typo or slight misunderstanding can stop your progress cold. Working with the support of your classmates and staff can make the difference between quickly resolving that sticking point versus hours of frustration wrestling on your own in the dark.</p>

<p>Each lab has a set of check-in questions that we want you to answer as you go. Touch base with the staff on your answers to confirm your understanding and resolve any confusion. The check-in questions are intentionally simple and your responses are not graded; we use them as a gauge of how you’re doing with the material so that we know better how to help guide you.</p>

<p>To
get the most out of lab, don’t set your goal at merely finishing the absolute minimum required in the
shortest possible time.  If you already have a good handle on the material, use the lab period to dive into further nooks and crannies or help out those peers who could benefit from your experience.  You should also
get to know the instructors.  They are masters of the craft, and you will learn
a lot by talking to them and asking them questions.  Any topic is fair game.</p>

<p>The combination of hands-on experimentation,
give and take with your peers,
and the expert guidance of our staff 
is what makes lab time truly special.
Your sincere participation can really accelerate your learning!</p>

<h2 id="prelab-preparation">Prelab preparation</h2>

<p>To prepare, please do the following before coming to lab:</p>

<ol>
  <li>Use the final checklist at the end of the <a href="/guides/install">installation guide</a> to confirm your laptop is ready for action.</li>
  <li>You will need an open USB-A port on your laptop to connect to the Pi. If your laptop requires a hub or adapter, be sure to bring it with you to lab.</li>
  <li>Review our guide to the <a href="/guides/unix/">Unix command line</a>.</li>
</ol>

<h2 id="lab-exercises">Lab exercises</h2>

<p>When you start lab, pull up the <a href="checkin">check-in questions</a> in your browser so you can refer to them as you go.</p>

<h3 id="0-clone-the-lab-repo">0. Clone the lab repo</h3>
<p>Before starting lab, do a <code class="highlighter-rouge">git pull</code> in your copy of the <code class="highlighter-rouge">cs107e.github.io</code> repository to ensure your courseware files are up to date.</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>$ cd ~/cs107e_home/cs107e.github.io
$ git pull
</code></pre></div></div>

<p>The lab materials are distributed in a separate git
repository. We recommend that you store your copy of the lab under your <code class="highlighter-rouge">cs107e_home</code>.  Change to that directory now and clone lab1:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>$ cd ~/cs107e_home
$ git clone https://github.com/cs107e/lab1
</code></pre></div></div>

<p><a name="step1"></a></p>
<h3 id="1-assemble-blink">1. Assemble blink</h3>
<p>Change to the lab subdirectory that contains the <code class="highlighter-rouge">blink</code> example and build the <code class="highlighter-rouge">blink</code> program using these commands:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>$ cd lab1/code/blink
$ arm-none-eabi-as blink.s -o blink.o
$ arm-none-eabi-objcopy blink.o -O binary blink.bin
</code></pre></div></div>

<p>If above commands execute without error, you are good to go!</p>

<h3 id="2-inventory-your-kit">2. Inventory your kit</h3>

<p>You will receive your CS107e Raspberry Pi kit when you arrive at lab.
Take a moment to identify all your parts and compare to the <a href="/guides/bom/">kit inventory</a> to ensure your kit is complete.</p>

<p>(Recall that a <a href="https://learn.sparkfun.com/tutorials/resistors">resistor</a>’s band colors tell its resistance: in this case,
10K or 1K ohms. Check out this <a href="http://www.digikey.com/en/resources/conversion-calculators/conversion-calculator-resistor-color-code-4-band">chart and calculator</a>.)</p>

<h3 id="3-power-your-breadboard">3. Power your breadboard</h3>

<p>Next you will wire up a simple circuit on your breadboard to light an LED. For a quick introduction or refresher on using a breadboard, read this 2-minute <a href="https://www.losant.com/blog/how-to-use-a-breadboard">breadboard tutorial</a>.
Which holes are connected to which other holes?
How are the power and ground rails connected?</p>

<p>Note that an LED is directional.
The longer lead is the anode and the shorter lead is the cathode.
The voltage from anode to the cathode should be positive.
If the polarity of voltages are switched, the LED will not light up.
A LED also needs a 1K current limiting resistor
otherwise it can literally 
<a href="https://www.youtube.com/watch?v=WLctUO1DGtw">blow up in a fiery, smoky extravaganza</a>!</p>

<p>In the photo below of our circuit,
we connected the cathode of the LED to the 1K resistor 
and then connected the other end of the resistor to the blue ground rail.
Note how the LED crosses over the middle of the breadboard.
To light up the LED, we need to apply power to the anode
and complete the circuit by connecting the cathode to GND.</p>

<p><img src="images/led.jpg" width="500" /></p>

<p>To check that the LED is working, you need to power the circuit.
We will draw power from your laptop
using a <em>USB to Serial Adapter</em> (hereafter referred to as just “USB-serial”).
This is the small black breakout board with a USB-A connector
on one side and a 6-pin header on the other side.
The USB connector is inserted into a USB-A port on your laptop. If your laptop does not have a USB-A port, you will need an adapter.</p>

<p>When wiring, electronics gurus use colored wires to indicate what type of
signal is being carried by that wire.
This makes debugging tangled wires much easier.
Generally, we will use the following conventions.</p>

<ul>
  <li>Black (GND)</li>
  <li>Red (5V)</li>
  <li>Orange (3.3V)</li>
  <li>Blue (host output)</li>
  <li>Green (host input)</li>
</ul>

<p>In this next step, we choose red and black jumpers because we are routing power and ground.</p>

<p>To provide power to your breadboard,
do the following steps in <strong>precisely this order</strong>.</p>

<ol>
  <li>
    <p>Pick out two female-male jumper cables, one red and one black.</p>
  </li>
  <li>
    <p>Connect the female ends of the jumpers to the header pins on the USB-serial breakout board. Connect the black jumper to the header labeled GND and the red jumper to the header labeled VCC.</p>
  </li>
  <li>
    <p>Connect the male ends of the jumpers to the breadboard. Plug the male end of the black jumper into the blue
ground rail. Plug the male end of the red jumper to the LED anode (longer leg).  Remember to include the 1k resistor in the circuit between the LED cathode (shorter leg) and GND.</p>
  </li>
  <li>
    <p>After double-checking that your circuit wiring is correct, you’re ready to apply power. Plug the USB connector of the USB-serial into your laptop.
A small led on the breakout board lights up to indicate that it has power.
The LED on the breadboard connected to the red jumper should also be lit.</p>
  </li>
</ol>

<!-- This partial will open warning/danger callout box  -->

<div style="background-color:#ffcccc; color:#993333; border-left: 5px solid #993333;margin: 5px 25px; padding: 5px;">

  <p><strong>Danger:</strong> Don’t have the USB-serial plugged in to your laptop
while you are fiddling with the wiring.
The breakout board provides power which means all the wires are live.
This can cause a short circuit, which could fry your Pi.</p>
</div>

<p>While the LED is lit, make the following measurements with the multimeter.</p>

<ul>
  <li>Measure and record the voltage across the resistor.</li>
  <li>Measure and record the voltage across the LED.</li>
</ul>

<p>Calculate the current flowing through the LED. You should now be able to answer the first <a href="checkin">check in question</a>.</p>

<h3 id="4-power-via-the-pi">4. Power via the Pi</h3>

<p>Identify the 40-pin GPIO header on the Raspberry Pi A+ board and orient it to match the <a href="/guides/images/pinout.pdf">pinout diagram</a> (this pinout diagram also available as a postcard in your kit and a poster on lab wall).  Read the pin labels on the diagram and identify two 5V power pins and two GND pins; you’ll use these pins in this step.</p>

<p>Re-wire your circuit to run power/ground from the USB-serial first to the Raspberry Pi and from there to the breadboard. Follow these steps:</p>

<ol>
  <li>Unplug the USB-serial from your laptop so that no power is flowing. Disconnect the jumpers between the USB-serial and breadboard.</li>
  <li>Connect power and ground from the USB-serial to the Raspberry Pi using two female-female jumpers. Use a black jumper to connect the GND of the USB-serial to a GND GPIO on the Pi. Similarly connect a red jumper for the 5V power.</li>
  <li>Connect power and ground from the Raspberry Pi to the breadboard using the two female-male jumpers.  The black jumper connects a GND GPIO to the blue ground rail on the breadboard. The red jumper connects a 5V GPIO to the LED anode.</li>
</ol>

<p>Power is now flowing from the USB-serial to the Raspberry Pi and then to the breadboard.</p>

<p><img src="images/piled.jpg" width="500" /></p>

<p>After double-checking your wiring, apply power by plugging the USB-serial in your laptop. All three LEDs should light: the one on the USB-serial, the red power LED on the Raspberry Pi, and the<br />
the LED on the breadboard. Your circuit is complete!</p>

<p>Replace your 1K resistor with a 10K resistor. How does the brightness of
the LED change? (You might want to coordinate with another group so
you can compare them side by side.) Why does it change?</p>

<h3 id="5-prepare-sd-card">5. Prepare SD card</h3>

<p>Your Raspberry Pi kit contains a microSDHC card (shown in the photo below on left alongside its adapter jacket). The Pi has a slot that accepts a micro-SD card on the underside of the circuit board (shown in the photo below right). When the Pi boots, it accesses the card to read the file named <code class="highlighter-rouge">kernel.img</code> and executes that program. You can change the program run by the Pi by writing a different <code class="highlighter-rouge">kernel.img</code> to the card.</p>

<p><img title="micro-sd with adapter" src="/guides/images/sd.kingston.jpg" width="200" style="display:inline" /><img title="micro-sd inserted in pi" src="/guides/images/sd.pi.jpg" width="200" style="float:right" /></p>

<p>To copy files to the SD card, mount it on your laptop.  If your laptop does not have a built-in SD card reader, you will need an external reader (we have a few to loan in lab).  An adapter “jacket” allows a micro-SD to be used in a full-size SD card reader. First insert the micro-SD card into its adapter, and then insert the adapter into the SD card slot on your laptop.</p>

<p><img src="/guides/images/sd.mac.jpg" alt="SD holder" /></p>

<p>When you insert the SD card it should mount automatically and the volume will show up in the Finder/File Explorer. 
By default, the SD card volume is named <code class="highlighter-rouge">NO NAME</code>.
You can change the name if you wish.</p>

<p>On a Mac, another way to confirm that the card is mounted is to list 
the mounted Volumes in your shell:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>$ ls /Volumes
Macintosh HD    NO NAME
</code></pre></div></div>

<p>On Windows, the SD card will show up in your File Explorer, but the WSL shell will not have access to it. Use File Explorer to copy/rename files on the SD card.</p>

<!-- This partial will open warning/danger callout box  -->

<div style="background-color:#ffffcc; color:#996633; border-left: 5px solid #996633;margin: 5px 25px; padding: 5px;">

  <p><strong>Note:</strong>  If your laptop doesn’t have an SD card slot or the card slot isn’t cooperating, ask your partner to use their computer or borrow a USB card reader from us. Configuring your SD card is a one-time task. You will not need to modify it again and in future will use the bootloader, so do whatever is most expedient to prep your SD card and move on.</p>
</div>

<p>The Raspberry Pi firmware files are stored in the firmware subdirectory of the courseware repository. Change to that directory and confirm you have these 4 files:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>$ cd ~/cs107e_home/cs107e.github.io/firmware
$ ls
blink-actled.bin   bootloader.bin  
bootcode.bin       start.elf
</code></pre></div></div>

<p>The <code class="highlighter-rouge">bootcode.bin</code> and <code class="highlighter-rouge">start.elf</code> files are needed to start up the Raspberry Pi. The two additional files
<code class="highlighter-rouge">blink-actled.bin</code> and <code class="highlighter-rouge">bootloader.bin</code> are programs.</p>

<p>The SD card needs an additional file named <code class="highlighter-rouge">kernel.img</code>. 
Normally, <code class="highlighter-rouge">kernel.img</code> is the operating system kernel you want to run
on the Pi, like Linux or Windows. But notice that we don’t give you a <code class="highlighter-rouge">kernel.img</code>! In this course, we will write our own program to take the place of the kernel, and put our program under the name <code class="highlighter-rouge">kernel.img</code>.</p>

<p>We choose to first run the <code class="highlighter-rouge">blink-actled.bin</code>. This program blinks the green activity (ACT) LED near the corner of the Raspberry Pi board.</p>

<p>Follow these steps in order:</p>

<ol>
  <li>Copy the four files from the firmware folder onto the SD card.
    <ul>
      <li>On Mac, you can use either do this in the shell or using Finder.</li>
      <li>On Windows, you can access the SD card only in in File Explorer, not the WSL shell. After changing to the firmware directory in your WSL shell, use the command <code class="highlighter-rouge">explorer.exe .</code> to show the current directory contents in File Explorer and you can copy those files to the SD card.</li>
    </ul>
  </li>
  <li>
    <p>On the SD card, make a copy of <code class="highlighter-rouge">blink-actled.bin</code> named  <code class="highlighter-rouge">kernel.img</code>.</p>
  </li>
  <li>
    <p>Confirm that your SD card has the following files:</p>

    <div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code> $ ls
 blink-actled.bin    bootloader.bin        start.elf
 bootcode.bin        kernel.img
</code></pre></div>    </div>

    <p>The three critical files for the Pi boot sequence are:</p>

    <p><code class="highlighter-rouge">bootcode.bin</code> to boot the GPU</p>

    <p><code class="highlighter-rouge">start.elf</code> to start the CPU</p>

    <p><code class="highlighter-rouge">kernel.img</code> the program for Pi to execute</p>

    <p>Any other files on the card are ignored by the Pi when booting.</p>
  </li>
  <li>
    <p>Eject the SD card. If Terminal prevents you from ejecting, type in <code class="highlighter-rouge">cd ..</code> to move to the parent folder and try ejecting again.</p>
  </li>
  <li>
    <p>Insert the micro-SD card into the slot on the bottom side of the Raspberry Pi board.</p>
  </li>
  <li>Power the Pi.  The green ACT LED on the Pi’s board
should start blinking. Ta-da! 🎉</li>
</ol>

<p>Keep this procedure for reconfiguring a fresh SD card in the back of your mind.
If you ever think your Pi is not working because of a hardware problem,
repeat these steps.
If the ACT LED doesn’t blink after booting, 
then something is wrong and you may need to replace the Pi with a working one.</p>

<h3 id="6-blink-breadboard-led">6. Blink breadboard LED</h3>

<p>Next, we are going to use the <code class="highlighter-rouge">blink</code> program (which pulses GPIO 20) in place of <code class="highlighter-rouge">blink-actled</code> (which pulses the on-board ACT LED at GPIO 47). Start by re-wiring your circuit. Use the Pi <a href="/guides/images/pinout.pdf">pinout diagram</a> to identify GPIO 20 and connect it to the anode of the LED on the breadboard.</p>

<p><img src="images/piled.gpio20.jpg" alt="GPIO 20 connected to LED" /></p>

<p>Next, update the files on your SD card. Carefully eject the micro-SD from your Pi, and mount it again on your laptop.</p>

<!-- This partial will open warning/danger callout box  -->

<div style="background-color:#ffcccc; color:#993333; border-left: 5px solid #993333;margin: 5px 25px; padding: 5px;">

  <p><strong>Take care!</strong> To eject the micro-SD from the Pi’s card slot, gently push the card in and allow it to spring back out. If you try to pull out the card by force, you can break the mechanism and potentially destroy your Pi.</p>
</div>

<p>Copy your <code class="highlighter-rouge">blink.bin</code> file (the one you assembled in <a href="#step1">step 1 of this lab</a>)
to your SD card and name it <code class="highlighter-rouge">kernel.img</code>, replacing the file that was previously there.</p>

<p>Eject the SD card and insert it into the Raspberry Pi.</p>

<p>When you boot your Pi, it should run the blink program which blinks the LED on your breadboard.</p>

<p>Hoorah, hoorah!🔥</p>

<h3 id="7-a-better-way-bootloader">7. A better way: bootloader</h3>

<p>Each time you change your code, you could repeat this process.
You would
power down your Pi, eject the SD card, 
insert the SD card into your laptop,
copy the new version of your code to <code class="highlighter-rouge">kernel.img</code>,
unmount and eject the SD card from your laptop,
insert it into the Pi,
and then power it up.
This quickly becomes tedious.
Even worse, the SD connectors are only designed to withstand
around 1000 insertions and deletions, after which they start to fail.</p>

<p>Instead, you can set up a serial connection between your laptop and the Pi and use a <strong>bootloader</strong> to transfer the program. The bootloader runs on your Pi
and listens on the serial connection. On your laptop, you run a script to send the program over the serial connection to the waiting 
bootloader. The bootloader receives the program and writes it to the
memory of the Pi, a process called “loading” the program. After the
program is loaded, the bootloader jumps to the start address of the program,
and the program begins to run.</p>

<p>To stop that program and start another, reset the Pi and use the bootloader again.  This is much more convenient way to run your newly compiled program than all that shuffling of SD cards. You will learn to love the bootloader!</p>

<p>First install the bootloader onto your SD card:</p>

<ol>
  <li>
    <p>Mount the SD card on your laptop. Make a copy of
<code class="highlighter-rouge">bootloader.bin</code> and name it <code class="highlighter-rouge">kernel.img</code>, replacing the program
you had there before.</p>
  </li>
  <li>
    <p>Eject the SD card and insert it into the Raspberry Pi. Now whenever you reset the
Pi with that micro-SD card installed, the bootloader will run.</p>
  </li>
</ol>

<p>To use the bootloader, you must set up the communication channel between your computer and the Pi.  The USB-serial that you are using to power your Pi also contains pins that can be used as a serial communication line.</p>

<p>The 6-pin header at the end of the USB-serial breakout board has two pins labeled for transmitting (TX) and receiving (RX).
The Pi also has a TX and RX pin (GPIO pins 14 and 15, respectively). Use the Raspberry Pi <a href="/guides/images/pinout.pdf">pinout diagram</a> to find these pins on the GPIO header.</p>

<p>Pick out two more female-female jumpers, one blue and one green. Use the blue jumper to connect the TX on the USB-serial to the RX on the Pi, and the green jumper to connect RX on the USB-serial to the TX on the Pi. As always, first unplug the USB-serial from your laptop before fiddling with your wiring.</p>

<!-- This partial will open warning/danger callout box  -->

<div style="background-color:#ffffcc; color:#996633; border-left: 5px solid #996633;margin: 5px 25px; padding: 5px;">

  <p><strong>Note:</strong> The connections run from one device’s TX to the other’s RX, and vice versa. Do <strong>not</strong> connect TX to TX and RX to RX!</p>
</div>
<p>Note that the pins on your USB-serial
may be in different positions or have different label names. Don’t just follow the picture blindly!
<img src="/guides/images/bootloader.cable.jpg" alt="Console cable" /></p>

<p>In the above photo, the green wire connects
the RX header pin on the USB-serial
to the Pi’s TX pin (GPIO 14).
The blue wire connects the TX header pin on the USB-serial
to the Pi’s RX pin (GPIO 15).</p>

<p>Plug in your USB-serial to reset your Pi. The bootloader should run on reset. When the bootloader is running, it signals that it is waiting to receive a program by repeatedly giving two short flashes of the green ACT LED on the Pi board. This “da-dum” is the heartbeat that tells you the bootloader is ready and listening. Look at your Pi now and observe this rhythm. Breathe in sequence with it for a moment to celebrate having achieved bootloader readiness.</p>

<p>Our Python script <code class="highlighter-rouge">rpi-install.py</code> runs on your laptop to send a program to the bootloader. Verify you have the proper version (1.1) from the following command:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>$ rpi-install.py -h
usage: rpi-install.py [-h] [-v] [-q] [-t T] [-p | -s] [port] file

This script sends a binary file to the Raspberry Pi bootloader. Version 1.1
...
</code></pre></div></div>

<p>Let’s try bootloading a program. On your laptop, change back to the <code class="highlighter-rouge">lab1/code/blink/</code>
directory where you assembled <code class="highlighter-rouge">blink.bin</code> in step 1.</p>

<p>Watch the ACT LED on your Pi to confirm you see the bootloader heartbeat. To load and run <code class="highlighter-rouge">blink.bin</code>, simply type:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>$ rpi-install.py blink.bin
Found serial port: /dev/cu.SLAB_USBtoUART
Sending `blink.bin` (72 bytes): .
Successfully sent!
</code></pre></div></div>

<p>While receiving a program, the bootloader turns on the ACT LED and holds it steady until transmission is complete. The bootloader then turns off the ACT LED and transfers execution to the received program. The <code class="highlighter-rouge">blink.bin</code> program should now blink the LED on your breadboard.</p>

<p>If you change your program and wish to reload it onto the Pi, you must first 
reset the Pi. What happens if you try to <code class="highlighter-rouge">rpi-install.py</code> a second time
after the bootloader has already loaded a program? Why does that happen?</p>

<p>One way to reset the Pi is to briefly cut power by unplugging the USB-serial
from your laptop,
and then plug it in again.
The Pi will restart into bootloader, ready to receive a new program.</p>

<p>Reset your Pi now and re-load the blink program. Hoorah, hoorah, hoorah!! 👏</p>

<h3 id="8-study-the-blink-program">8. Study the blink program</h3>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>.equ DELAY, 0x3F0000

// configure GPIO 20 for output
ldr r0, FSEL2
mov r1, #1
str r1, [r0]

mov r1, #(1&lt;&lt;20)

loop: 

// set GPIO 20 high
ldr r0, SET0
str r1, [r0] 

// delay
mov r2, #DELAY
wait1:
    subs r2, #1
    bne wait1

// set GPIO 20 low
ldr r0, CLR0
str r1, [r0] 

// delay
mov r2, #DELAY
wait2:
    subs r2, #1
    bne wait2

b loop

FSEL0: .word 0x20200000
FSEL1: .word 0x20200004
FSEL2: .word 0x20200008
SET0:  .word 0x2020001C
SET1:  .word 0x20200020
CLR0:  .word 0x20200028
CLR1:  .word 0x2020002C
</code></pre></div></div>

<p>If there is anything you don’t understand about this program,
ask questions of your partner and others.</p>

<p>Do the following exercises:</p>

<ul>
  <li>
    <p>Look at the bytes in the <code class="highlighter-rouge">blink.bin</code> you assembled earlier by
running <code class="highlighter-rouge">hexdump blink.bin</code> at a shell in the <code class="highlighter-rouge">blink</code> folder.</p>

    <p>(<code class="highlighter-rouge">hexdump</code> is a command that prints the bytes in a file in a
human-readable form. You can run <code class="highlighter-rouge">man hexdump</code> to learn more. What are
the numbers at the beginning of each line <code class="highlighter-rouge">hexdump</code> outputs?)</p>

    <p>Find the first occurrence of <code class="highlighter-rouge">e3</code>. What is the byte offset of <code class="highlighter-rouge">e3</code>
relative to the start of the file?</p>
  </li>
  <li>
    <p>Change the program such that the blink rate slows down by a factor
of 2.</p>

    <p>Modifying the program is a multi-step process. First
you edit <code class="highlighter-rouge">blink.s</code> in a text editor, then use the
commands in step 1 again to build <code class="highlighter-rouge">blink.bin</code> from it, and then
   unplug and replug to reset your Pi and use <code class="highlighter-rouge">rpi-install.py</code> to send your new <code class="highlighter-rouge">blink.bin</code> to run on the Pi. Make sure you understand why
these steps are all necessary.</p>

    <p>Now perform experiments to determine how many instructions per second the
Raspberry Pi executes.</p>
  </li>
</ul>

<p><a name="button"></a></p>
<h3 id="9-add-a-button-optional">9. Add a button (optional)</h3>

<p>This last part is optional.
You do not need to use buttons for Assignment 1,
but you will for Assignment 2.
There are no check-in questions for this exercise.</p>

<p>Measure the resistance across the pushbutton legs using a 
multimeter and figure out which pins are always connected 
and which become connected when the button is pushed.
Use these observations to determine how to position the button correctly on the breadboard. 
The pushbutton needs a 10K pull-up resistor to the red power rail.
Verify that the resistor is 10K Ohms using the multimeter.
Measure the voltage at the pin, and measure it again when you push the button.</p>

<p>Here is a program that reads a button and turns on or off the LED depending on
whether the button is pressed.</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>// configure GPIO 10 for input
ldr r0, FSEL1
mov r1, #0
str r1, [r0]

// configure GPIO 20 for output
ldr r0, FSEL2
mov r1, #1
str r1, [r0]

// bit 10
mov r2, #(1&lt;&lt;10)

// bit 20
mov r3, #(1&lt;&lt;20)

loop: 
    // read GPIO 10 
    ldr r0, LEV0
    ldr r1, [r0] 
    tst r1, r2
    beq on // when the button is pressed (goes LOW), turn on LED
    
    // set GPIO 20 low
    off:
        ldr r0, CLR0
        str r3, [r0]
        b loop

    // set GPIO 20 high
    on:
        ldr r0, SET0
        str r3, [r0]
        b loop

FSEL0: .word 0x20200000
FSEL1: .word 0x20200004
FSEL2: .word 0x20200008
SET0:  .word 0x2020001C
SET1:  .word 0x20200020
CLR0:  .word 0x20200028
CLR1:  .word 0x2020002C
LEV0:  .word 0x20200034
LEV1:  .word 0x20200038
</code></pre></div></div>

<p>To run this program, connect the button to GPIO 10.
Make sure the jumper is
connected to the correct pin on the Raspberry Pi.
Also, make sure the pull-up resistor is properly installed on the breadboard.</p>

<p>Challenge yourself to understand
what each line of code accomplishes and why it works as expected.
Feel free to add your own code annotations if that helps.</p>

<p>Here are a few questions to test your knowledge.  To confirm your answers, you
will have to read the <a href="/readings/BCM2835-ARM-Peripherals.pdf">Broadcom peripheral
manual</a>,
or ask someone who knows the answer.</p>

<ul>
  <li>
    <p>What does the peripheral register with the address 0x20200034 return?</p>
  </li>
  <li>
    <p>Why does the input value go to 0 (LOW) when the button is pressed?</p>
  </li>
  <li>
    <p>How does the Pi know which branch to jump to when it reaches <code class="highlighter-rouge">beq on</code>?</p>
  </li>
</ul>

<h2 id="check-in-with-ta">Check in with TA</h2>

<p>Each pair of students should periodically touch base with the TA as you answer the <a href="checkin">check-in questions</a>. The TA will
verify your understanding and can help with any unresolved issues.</p>

<p>Remember that the goal of the lab is not to answer exactly and only these questions – it’s to work through the material. The questions are an opportunity to self-test your understanding and confirm with us.</p>

<p>It’s okay if you don’t completely finish all of the exercises during
lab; your sincere participation for the full lab period is sufficient
for credit.  However, if you don’t finish, we highly encourage you to
work those parts to solidify your knowledge of this material before
moving on. In particular, having successfully completed this lab is a necessary step before tackling this week’s assignment.</p>


  </div>
  <div class="toc-column col-lg-2 col-md-2 col-sm-2 hidden-xs">
    <div id="toc" class="toc" data-spy="affix" data-offset-top="0"></div>
  </div> 
</div>

  <script src="/_assets/tocbot.min.js"></script>
  <link rel="stylesheet" href="/_assets/tocbot.css">

  <script>
    tocbot.init({
      // Where to render the table of contents.
      tocSelector: '#toc',
      // Where to grab the headings to build the table of contents.
      contentSelector: '#main_for_toc',
      // Which headings to grab inside of the contentSelector element.
      headingSelector: 'h2, h3, h4',
    });
  </script>



  </div>
  <div class="footer navbar-default navbar-static-bottom">
    <p style="font-size: 65%; color:green; text-align:center;">
      <i>CS107e Winter 2020 &middot; Site generated 2020-04-19 20:35</i>
  </p>
</div> 
</body>

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</html>