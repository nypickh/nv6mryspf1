<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/_assets/site.css" rel="stylesheet">
    <link href="/_assets/favicon.png" rel="icon" type="image/png">
    <title>CS107E Course Policies</title>
  </head>

<body>
  <!-- Include this HTML partial to set up navbar -->

<nav class="navbar navbar-inverse" role="navigation">
  <div class="navbar-header">
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
    <!-- hamburger -->
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>

  <img class="navbar-brand navbar-brand-image" src="/_assets/berry.png" >
  <a class="navbar-brand" href="/">CS107e Winter 2020</a>
  </div>

  <div class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/">Home</a></li>
      <li><a href="/assignments">Assignments</a></li>
      <li><a href="/labs">Labs</a></li>
      <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            Resources
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/about/">About the course</a></li>
            <li><a href="/policies/">Policies</a></li>
            <li><a href="/schedule/">Schedule</a></li>
            <li><a href="/guides/">Guides</a></li>
            <li><a href="/project_gallery/">Project gallery</a></li>
            <li><a href="/demos/">External demos</a></li>
            <li><a href="/resources/">External resources</a></li>
          </ul>
        </li>
      <li><a href="https://github.com/cs107e/cs107e.github.io">Repository</a></li>
    </ul>
  </div>
</nav>


  <div class="container" style="max-width:55em;">
    <h1 class="title">Course Policies</h1><hr>
    
  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" id="main_for_toc">
    <h2 id="grading-policy">Grading Policy</h2>
<p>Your course grade is a combination of your participation in lab and your work on the assignments and final project.</p>

<ul>
  <li>There are 9 weekly labs, graded for participation on a credit/no-credit basis.</li>
  <li>There are 7 weekly assignments. Each assignment will have a required basic part and optional extensions.  You are required to complete the basic part, and are encouraged to attempt some of the extensions.</li>
  <li>The course culminates with a final project where you design and implement a system of your choosing.</li>
</ul>

<p>If you have your sights set on earning an <strong>A</strong> course grade, you will need consistently <strong>outstanding</strong> work:</p>
<ul>
  <li>Attend all labs</li>
  <li>Assignments
    <ul>
      <li>Basic requirements are complete and fully-functional for all assignments</li>
      <li>In addition, 3 or more assignment extensions successfully completed</li>
      <li>Earn the full system bonus for using all your own code on the last assignment</li>
      <li>Code quality (style and tests) trending to <code class="highlighter-rouge">+</code></li>
    </ul>
  </li>
  <li>Outstanding final project, excellent execution</li>
</ul>

<p>For a <strong>B</strong> course grade, we expect consistently <strong>solid</strong> work:</p>
<ul>
  <li>Attend all labs</li>
  <li>Assignments
    <ul>
      <li>Basic requirements are complete and mostly fully-functional for all assignments</li>
      <li>At least 1 assignment extension attempted/completed or full system bonus achieved</li>
      <li>Code quality (style and tests) trending to <code class="highlighter-rouge">ok</code></li>
    </ul>
  </li>
  <li>Good final project, satisfactory execution</li>
</ul>

<p>Work that is not completed satisfactorily will earn grades <strong>C and below</strong>.</p>

<h2 id="late-policy">Late Policy</h2>

<p>Hofstadter’s Law: “It always takes longer than you think, even when you take
Hofstadter’s Law into account.”</p>

<p>The rules:</p>

<ol>
  <li>
    <p>The cutoff for on-time submission is typically <b> 6:00 pm</b> of the due date. Late
days are counted in 24-hour periods. Submitting anytime after  6:00 pm
and before  6:00 pm of the next day is one day late, and so on. We use the
timestamp of your final git commit as the time of submission.</p>
  </li>
  <li>
    <p>You are given <strong>4 “grace days”</strong> (self-granted extensions) which you can
use to give yourself extra time without penalty.</p>
  </li>
  <li>
    <p>Instructor-granted extensions are only considered <strong>after all grace days
are used</strong> and only given in exceptional situations.</p>
  </li>
  <li>
    <p>Late work handed in when you have run out of grace is <strong>discounted 10% per
day late</strong>. It is always to your advantage to complete all the assignments
even if they are late.</p>
  </li>
  <li>
    <p>Late submissions (penalty or not) are not accepted after the hard deadline,
which is <strong>4 days past the original due date,</strong> but may be restricted to
fewer days on a per-assignment basis.</p>
  </li>
  <li>
    <p>The final project must be submitted on time.</p>
  </li>
</ol>

<h2 id="collaboration-policy">Collaboration Policy</h2>

<p><em>Adapted from CS107 policy by Julie Zelenski, some wording borrowed from a
collaboration policy distributed by Brown CS.</em></p>

<p>The programming assignments are an integral part of the course learning
experience. We have a great set of projects lined up for you and the time you
spend designing your own solution and working toward its successful
implementation can be fun, challenging, illuminating, time-consuming,
rewarding, and frustrating. Your pride upon finishing is a fantastic high and
your efforts earn you powerful skills and deep understanding. Don’t cheat
yourself out of this incredible learning opportunity! Borrowing someone else’s
design, building on another’s code, being lead by another person, and other
such “shortcuts” jeopardize the chance to develop your own mastery and
compromise your integrity.</p>

<p>This document is designed to make clear our expectations and restrictions on
collaboration. We expect your assignment submissions to be your own independent
effort and not be derived from the thoughts and work of others. You should do
your own thinking, your own design, your own coding, and your own debugging.
This doesn’t mean that you can’t ask questions or get help when you get stuck,
but any help must remain within acceptable limits, as detailed in the concrete
examples given below.</p>

<p>All of you should be familiar with the principles of the <a href="https://communitystandards.stanford.edu/student-conduct-process/honor-code-and-fundamental-standard#honor-code">Stanford Honor
Code</a>. The <a href="http://csmajor.stanford.edu/HonorCode.shtml">CS
department Honor Code policy</a>
further explains how it applies in CS courses. Students are to uphold their
obligations to do honorable work and encourage others to do the same. On our
part, we will treat students with trust and respect.</p>

<h3 id="assistance-that-is-allowed">Assistance that is allowed</h3>

<p>These things are encouraged and allowed for all students and require no citation:</p>

<h4 id="discussing-course-topics">Discussing course topics</h4>

<p>You may freely talk through any material from the lectures,
labs, readings, and course website.</p>

<h4 id="discussing-the-language-libraries-and-tools">Discussing the language, libraries, and tools</h4>

<p>For example: “What does the keyword static mean? How do I view my git
history? What does this ARM instruction do?”</p>

<h4 id="clarifying-an-assignment-specification">Clarifying an assignment specification</h4>

<p>For example: “Do the results have to be sorted? What is the
expected response if the input is empty?”</p>

<h4 id="sharing-generic-advice-and-techniques">Sharing generic advice and techniques</h4>

<p>For example: “I test each function right after I finish writing
it. When my program crashes, I first look at the stack trace
in the debugger. I used a logic analyzer to study the signals in my circuit.”</p>

<h4 id="use-of-external-resources-for-background-information">Use of external resources for background information</h4>

<p>You may search for and use external resources (web sites, blogs,
forums, etc.) for information on course topics, as reference
material on the programming language and libraries, and to
resolve technical difficulties in use of the tools.</p>

<h4 id="any-discussion-between-student-and-the-staff">Any discussion between student and the staff</h4>

<p>You are welcome to discuss any aspects of design, code, or
debugging with the course staff. They are the best folks to
talk to because they are knowledgeable about all the material
and know how to help you without overly influencing or leading you.</p>

<h3 id="assistance-that-must-be-cited">Assistance that must be cited</h3>

<p>Discussion and advice specific to the particulars of an
assignment should be cited. The particulars include such things
as the program’s design, data structures, choice of algorithms,
implementation strategies, testing, and debugging. Some examples:</p>

<h4 id="discussing-your-assignment-design">Discussing your assignment design</h4>

<p>Design is a crucial part of the programming process, and we
want you to learn to work through design problems on your own.
Only after you completed your own independently-conceived design,
you may compare and contrast with a peer who has also completed
their own design. You both must cite this discussion and note
any ideas taken away from it.</p>

<h4 id="helping-another-student-to-debug">Helping another student to debug</h4>

<p>A student might describe symptoms to a peer who
responds with suggestions (e.g. “it sounds like you
might have forgotten to terminate the string” or  have you tried running under the gdb simulator?”). If you receive debugging help that was essential to your progress, please cite it.</p>

<h4 id="sharing-test-strategies-or-inputs">Sharing test strategies or inputs</h4>

<p>If you discuss strategies for testing or jointly brainstorm test
inputs (e.g. “be sure to verify handling when index is out of range”), this collaboration should be cited.</p>

<h4 id="what-is-an-appropriate-citation">What is an appropriate citation?</h4>

<p>A citation should be specific, complete, and truthful. Clearly identify the source of the help/discussion (person’s
name, book title, URL), describe the nature and extent of the
assistance, and indicate how it influenced
your submission.</p>

<h3 id="assistance-that-is-not-allowed">Assistance that is NOT allowed</h3>

<p>Discussions should never become so detailed that they involve jointly
writing or exchanging/sharing passages of code. Your code must
represent your original, independent work and it should not be
developed in conjunction with or derived from anyone else’s. You
should never be intimate with another’s code nor allow others to
be intimate with yours. Here are specific examples of unpermitted
aid:</p>

<h4 id="copying-code">Copying code</h4>

<p>It is an act of plagiarism to submit work which is copied or
derived from the work of others and represent it as your own.
It does not matter if the plagiarized source was a current or
former student, outside the course, or found on the web, or
whether the amount was an entire program or just a small part;
in all cases, it is unacceptable. You should never be in
possession of anyone’s else code, whether in printed, written,
or electronic form.</p>

<h4 id="reviewing-the-codedesign-of-another">Reviewing the code/design of another</h4>

<p>You are not permitted to have another person “walk you through”
their approach nor may you use their work “as a reference” to
look at if you get stuck. This prohibition applies to both code
and design, to isolated passages as well as the entire program,
and whether the review is conducted verbally or in
printed/written/electronic form.</p>

<h4 id="joint-developmentdebugging">Joint development/debugging</h4>

<p>You are not permitted to work with another to jointly develop
a design, write code, or debug. Two students should never be
working together on a passage of code/pseudocode whether on
paper, on a whiteboard, or in editor/debugger of a shared
computer.</p>

<h4 id="use-of-external-resources-for-assignment-specific-code">Use of external resources for assignment-specific code</h4>

<p>You should not be searching external resources for solutions,
whether in the form of code, pseudocode, or design. Should you
find full/partial solutions anyway, you are to turn away and
report the location to us. A submission must not be adopted
from or influenced by study of any external resource.</p>

<h4 id="sharing-your-codedesign">Sharing your code/design</h4>

<p>You must not share your code with individual students nor
publicly broadcast it. The repositories we create for you on Github, 
will be private and we expect that you will 
keep them so. Even after the course ends, you are expected to take reasonable security precautions
to maintain your work privately. If we request that you
remove/protect code that has been improperly shared, you are
expected to comply in a timely manner.</p>

<h3 id="integrity-as-community">Integrity as community</h3>

<p>The Honor Code is a powerful assertion that we as a community proudly
dedicate ourselves to upholding the highest standards of academic
integrity. The vast majority will do right by CS107E – we ask a lot
of you and you will consistently meet those challenges by creating
work that authentically represents your own effort. We demonstrate
our respect and appreciation for your honor and efforts by doing
our part to make absolutely clear our expectations and hold accountable
those students who act in violation.</p>

<h3 id="common-questions-about-collaboration">Common questions about collaboration</h3>

<h4 id="can-i-include-my-solution-code-in-my-portfolio-for-internshipscholarship-applications">Can I include my solution code in my portfolio for internship/scholarship applications?</h4>

<p>We’re delighted to hear that you are proud of your work and want
to show it off! We are supportive, but ask that you contain the
code to the intended audience. You may share your code via direct
communication, privately-maintained repositories, restricted access,
or another controlled channel. Making your code
freely accessible on the web or maintaining a public repository at
a code-sharing site will be a temptation for others who would exploit
it. This is a bad situation we would much rather prevent than
prosecute.</p>

<h4 id="what-does-it-mean-to-have-third-party-responsibility-under-the-honor-code">What does it mean to have “third-party responsibility” under the Honor Code?</h4>

<p>The students and faculty agree to work together to establish and
maintain standards for honorable work. Students are not the primary
enforcers of the policy, but they give meaning and value to the
Honor Code by showing their respect for it and encouraging others
to the same. You are to model appropriate behavior in word and deed
and should decline to aid or support another’s actions in violation.
If asked by another to act inappropriately or you observe something
improper, take reasonable action to rectify the problem: draw
attention to it, ask the student to stop, remove the temptation,
or alert the course staff.</p>

<h4 id="can-you-further-clarify-appropriate-and-inappropriate-use-of-external-resources">Can you further clarify appropriate and inappropriate use of external resources?</h4>

<p>External resources can be used to research course topics and answer
general questions, e.g. review 2’s complement addition, ask “does
strncpy null-terminate the destination string?”, or learn how to
use a gdb watchpoint. It is not okay to use external
resources to obtain solutions/answers to assignment-specific tasks.
The prohibition applies to solutions in any form (e.g. design,
algorithm, code, pseudocode) and whether a complete solution or
just part. A few examples of inappropriate use: searching for code
to implement portions of the assignment, adopting pseudocode found
on Wikipedia for an assignment-specific algorithm, or posting a
broken passage of your assignment code on StackOverflow and asking
others to debug it. When you have assignment-specific issues, the
appropriate resource to tap into is us – come by office hours,
post to the forum or send us email. We’re happy to help!</p>

<h4 id="im-in-tough-situation-overloaded-falling-behind-stressed-and-my-panic-has-me-considering-less-than-honorable-ways-to-finish-my-assignment-help">I’m in tough situation (overloaded, falling behind, stressed) and my panic has me considering less-than-honorable ways to finish my assignment. Help!</h4>

<p>As much as we all intend and aspire to be worthy, in the heat of
the moment with mounting pressures and the coincidence of opportunity,
temptation can weaken resistance. If you find yourself vulnerable,
I ask you to please, please, please step away from any immediate
action. Taking a break or getting a good night’s sleep may be enough
to restore your sense of perspective. Even better would be to reach
out to me or someone else you trust and ask for guidance in working
through a bad situation. There are always other options. You can
ask for more help, you can take a late day even if it means a
penalty, you can submit a program that’s known to be imperfect, or
you can not submit a program at all. Maybe you can find relief by
allowing yourself to reset your expectations about the course grade
you’ll earn, changing the grading option to remove pressure, or
even withdrawing from the course. Even the most drastic of these
options has consequences that are small potatoes compared to
compromising your character and facing judicial charges. Please
come to me and I promise to support you in finding a way through
your situation that will maintain your integrity.</p>


  </div>
  <div class="toc-column col-lg-2 col-md-2 col-sm-2 hidden-xs">
    <div id="toc" class="toc" data-spy="affix" data-offset-top="0"></div>
  </div> 
</div>

  <script src="/_assets/tocbot.min.js"></script>
  <link rel="stylesheet" href="/_assets/tocbot.css">

  <script>
    tocbot.init({
      // Where to render the table of contents.
      tocSelector: '#toc',
      // Where to grab the headings to build the table of contents.
      contentSelector: '#main_for_toc',
      // Which headings to grab inside of the contentSelector element.
      headingSelector: 'h2, h3, h4',
    });
  </script>



  </div>
  <div class="footer navbar-default navbar-static-bottom">
    <p style="font-size: 65%; color:green; text-align:center;">
      <i>CS107e Winter 2020 &middot; Site generated 2020-04-19 20:35</i>
  </p>
</div> 
</body>

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</html>